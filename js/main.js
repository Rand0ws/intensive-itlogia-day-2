document.addEventListener('DOMContentLoaded', () => {
  // Scroll smooth
  const links = document.querySelectorAll('.menu-item a[href^="#"]');

  links.forEach(item => {
    item.addEventListener('click', e => {
      const id = item.getAttribute('href').slice(1);
      const temp = document.getElementById(id);

      e.preventDefault();
      temp.scrollIntoView({ behavior: 'smooth' });
    });
  });

  const btnViewMenu = document.querySelector('#main-action-button');
  const products = document.querySelector('#products');

  btnViewMenu.addEventListener('click', () => {
    products.scrollIntoView({ behavior: 'smooth' });
  });

  const buttons = document.querySelectorAll('.product-button');
  const order = document.querySelector('#order');

  buttons.forEach(item => {
    item.addEventListener('click', () => {
      order.scrollIntoView({ behavior: 'smooth' });
    });
  });

  // Change currency
  const switchCurrency = document.querySelector('.currency');
  const productPrices = document.querySelectorAll('.products-item-price');

  switchCurrency.addEventListener('click', () => {
    const currentCurrency = switchCurrency.textContent;
    let newCurrency = '$';
    let coefficient = 1;

    if (currentCurrency === '$') {
      newCurrency = '₽';
      coefficient = 80;
    } else if (currentCurrency === '₽') {
      newCurrency = 'BYN';
      coefficient = 3;
    } else if (currentCurrency === 'BYN') {
      newCurrency = '€';
      coefficient = 0.9;
    } else if (currentCurrency === '€') {
      newCurrency = '¥';
      coefficient = 6.9;
    }

    switchCurrency.textContent = newCurrency;

    productPrices.forEach(item => {
      const newPrice = +(Number.parseFloat(item.dataset.basePrice) * coefficient).toFixed(1);

      item.textContent = `${newPrice} ${newCurrency}`;
    });
  });

  // Form validate
  const burger = document.querySelector('#burger');
  const name = document.querySelector('#name');
  const phone = document.querySelector('#phone');
  const btnOrderSubmit = document.querySelector('#order-action');

  btnOrderSubmit.addEventListener('click', e => {
    let hasError = false;
    const inputs = [burger, name, phone];

    e.preventDefault();

    inputs.forEach(item => {
      if (!item.value) {
        item.parentElement.style.background = 'red';
        hasError = true;
      } else {
        item.parentElement.style.background = '';
      }
    });

    if (!hasError) {
      inputs.forEach(item => {
        item.value = '';
      });

      alert('Спасибо за заказ! Мы скоро свяжемся с Вами!');
    }
  });
});
